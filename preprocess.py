#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import cv2, numpy, os, argparse
from typing import List,Sequence

def image_to_sample(img: numpy.ndarray) -> Sequence[int]:
	arr = numpy.asarray(img)
	graysum = [0 for i in range(36)]
	total = 0
	for i in range(arr.shape[0]):
		for j in range(arr.shape[1]):
			if arr[i, j, 0] > 0:
				graysum[(i // 5) * 6 + j // 5] += 1
				total += 1
	
	for i in range(len(graysum)):
		graysum[i] = graysum[i] / total
	return graysum

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("image_dir")
	parser.add_argument("-o", required=True)
	args = parser.parse_args()
	
	trait_dir = args.image_dir
	output_dir = args.o
	char_dir = os.listdir(trait_dir)
	
	fp = open(output_dir, mode = "w")
	
	for d in sorted(char_dir):
		path = trait_dir + "/" + d
		print("%s" % d)
		for f in os.listdir(path):
			img_file = trait_dir + "/" + d + "/" + f
			img = cv2.imread(img_file, 1)
			sam_point = image_to_sample(img)
			fp.write(str(d) + ":")
			fp.write(",".join([str(n) for n in sam_point]))
			fp.write('\n')
	
	fp.close()

