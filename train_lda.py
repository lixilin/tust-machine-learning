#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import math, argparse, numpy as np, matplotlib.pyplot as plt
 
def fisher(w1, w2):	
	w1=np.mat(w1).T
	w2=np.mat(w2).T
 
	sz1 = np.size(w1,1)
	sz2 = np.size(w2,1)
	
	# 计算均值
	m1 = np.mean(w1, axis=1)
	m2 = np.mean(w2, axis=1)
  
	# 计算样本内离散度s1、s2和总类内离散度矩阵 Sw
	s1 = np.zeros((w1.shape[0],w1.shape[0]))
	n, m = w1.shape
	for i in range(m):
		tmp = w1[:,i] - m1
		s1 = s1 + tmp * tmp.T
	s2 =  np.zeros((w2.shape[0],w2.shape[0]))
	n, m = w2.shape
	for i in range(m):
		tmp = w2[:,i] - m2
		s2 = s2 + tmp * tmp.T
	sw = (sz1*s1 + sz2*s2)/(sz1+sz2)
 
	# 计算样本间离散度 sb
	sb = (m1 - m2) * (m1 - m2).T
 
	# 计算w*
	w_star = np.linalg.pinv(sw) * (m1-m2)
	res1=0
	for i in range(sz1):
		res1 = res1 + w1[:,i].T*w_star
	res1/=sz1
	res2=0
	for i in range(sz2):
		res2 = res2 +w2[:,i].T*w_star
	res2/=sz2
	return -(res1*sz1+res2*sz2)/(sz1+sz2), w_star

def perceptron(w1, w2):
	d = len(w1[0])
	m = max(len(w1), len(w2))
	W = np.array([1] * (d + 1))
	cont = True
	c = 0
	while cont:
		cont = False
		c += 1
		if c > 300:
			break
		for i in range(m):

			if i < len(w1):
				np_w1 = np.append(np.array(w1[i]), 1)
				t1 = np.dot(W, np_w1)
				if t1 <= 0:
					W = W + 10 * np_w1
					cont = True
			if i < len(w2):
				np_w2 = np.append(np.array(w2[i]), 1)
				t2 = np.dot(W, np_w2)
				if t2 >= 0:
					W = W - 5 * np_w2
					cont = True
	return W[-1], W[0:-1]
	
if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument("-o", required=True)
	parser.add_argument("-a", required=True, help='0 for Fisher and 1 for Perception')
	parser.add_argument("sample_file")
	args = parser.parse_args()
	assert args.a == '0' or args.a == '1'

	sample_file = args.sample_file
	arg_file = args.o
	fp = open(sample_file, "r")
	samples = {}
	for line in fp:
		char_id, vec_str = line.split(":")
		point = [float(n) for n in vec_str.split(",")]
		if char_id not in samples:
			samples[char_id] = []
		samples[char_id].append(point)

	fp.close()

	fp = open(arg_file, "w")

	key_list = list(samples.keys())
	total = (len(key_list) - 1) * len(key_list) // 2
	count = 0
	for i in range(0, len(key_list) - 1):
		for j in range(i + 1, len(key_list)):
			w1 = samples[key_list[i]]
			w2 = samples[key_list[j]]
			
			w0,w_star = fisher(w1, w2) if args.a == '0' else perceptron(w1, w2)
			w_star_str = ','.join([str(round(float(num), 6)) for num in np.asarray(w_star)])
			fp.write("%s,%s:%s:%s\n" % (key_list[i], key_list[j], str(round(float(w0), 6)), w_star_str))
			print("%d/%d" % (count, total))
			count = count + 1
	fp.close()

