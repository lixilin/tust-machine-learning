LDA_FILE = data/train_lda_out.txt
IMAGE_DIR = data/images_out
LABEL_FILE = label.txt
FONT_DIR = fonts
SAMPLE_FILE = data/samples.txt


all: test

test: $(LDA_FILE)
	./test.py -l $(LABEL_FILE) -t $< test_images
$(IMAGE_DIR):
	./makeimgs.py -l $(LABEL_FILE) -f $(FONT_DIR)/zh -o $@
	./makeimgs.py -l $(LABEL_FILE) -f $(FONT_DIR)/latin -o $@
$(SAMPLE_FILE): $(IMAGE_DIR)
	./preprocess.py $< -o $@
$(LDA_FILE): $(SAMPLE_FILE)
	./train_lda.py $< -a 1 -o $@

clean:
	$(RM) -r data/*
.PHONY: all test clean
